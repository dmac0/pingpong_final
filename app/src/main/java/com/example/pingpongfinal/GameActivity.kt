package com.example.pingpong

import com.example.pingpongfinal.R
import com.example.pingpong.PingPongGame.Settings
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class GameActivity : AppCompatActivity() {

    lateinit var settings: Settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settings = intent.getSerializableExtra("settings") as Settings
        setContentView(R.layout.activity_game)
    }

    override fun onRestart() {
        super.onRestart()
        this.onDestroy()
        this.onCreate(null)
    }


}